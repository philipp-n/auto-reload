<?php

$reloadingAmount = (int)$_GET['reloading-amount'];

$reload = false;

foreach (explode(",", $_GET['watch']) as $filePath) {

    $cacheDirTitle = str_replace("/", "..", $filePath);
    $cacheDirPath = dirname(__DIR__) . "/tmp/cache/$cacheDirTitle";
    $cacheFilePath = "$cacheDirPath/watch.txt";
    $cacheCountFilePath = "$cacheDirPath/watch_count.txt";

    if (!file_exists($cacheDirPath)) {
        exec("mkdir -p $cacheDirPath"); // @todo  Linux based (test with other OS)
    }
    if (!file_exists($cacheFilePath)) {
        file_put_contents($cacheFilePath, "Empty");
    }
    if (!file_exists($cacheCountFilePath)) {
        file_put_contents($cacheCountFilePath, 0);
    }


    $existingContents = file_get_contents($cacheFilePath);
    $contents = file_get_contents($filePath);
    $countWatch = (int)file_get_contents($cacheCountFilePath);

    if ($existingContents !== $contents) {

        file_put_contents($cacheFilePath, $contents);

        if ($reloadingAmount === 1) {
            file_put_contents($cacheCountFilePath, 0);
        } else {
            file_put_contents($cacheCountFilePath, ++$countWatch);
        }

        $reload = true;
        break;
    } elseif ($reloadingAmount !== 1 && $countWatch > 0 && $countWatch < $reloadingAmount - 1) {
        file_put_contents($cacheCountFilePath, ++$countWatch);
        $reload = true;
        break;
    } elseif ($reloadingAmount !== 1 && $countWatch >= $reloadingAmount - 1) {
        file_put_contents($cacheCountFilePath, 0);
        $reload = true;
        break;
    }

    file_put_contents($cacheCountFilePath, 0);
}

echo json_encode($reload);
