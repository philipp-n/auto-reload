# ◦ Requirements

- Vanilla JavaScript (ECMA 5)
- PHP

# ◦ Install

### ▸ `composer require philippn/auto-reload --dev`

# ◦ USE

### Basic

```html
<script src="/vendor/philippn/auto-reload/js/watch.js" watch="/var/www/html/index.php"></script>
```

### More parameters

```html
<script src="/vendor/philippn/auto-reload/js/watch.js"
        reloading-msg="Refreshed"
        timeout="1500"
        watcher-url="/custom-path/watch.php"
        url-request="key=value"
        reloading-amount="2"
        watch="
        /var/www/html/dev/index.php
        /var/www/html/dev/script.js
        /var/www/html/dev/style.css
"></script>

<link rel="stylesheet" href="/vendor/philippn/auto-reload/css/styles.css">

<div id="auto-reload__play-pause" onclick="autoReload.playPauseAutoReload()"></div>
<!--
Grey image color: by default
Black: style="filter:invert(0%)" 
White: style="filter:invert(100%)" 
-->
```

As soon as you update any file from `watch` param, page is reloaded N times (`reloading-amount`)

---

`reloading-amount` = defaults to `1`

`reloading-msg` = Displayed when page is reloaded specified amount of times, __'Auto reloaded'__ is default

`timeout` = time between watching requests, in milliseconds. Defaults to `2500`

`watcher-url` = custom watcher url, defaults to `/vendor/philippn/auto-reload/php/watch.php`

`url-request` = Any additional GET request string (without `?` at the beginning)

# ◦ CACHE

### Clear cache with

### ▸ `vendor/bin/auto-reload`


# ◦ USE WITHOUT composer

Download ZIP from 

```
https://philippn.com/vendor/auto-reload/auto-reload.zip
```

Unpack it inside your project

Add this code snippet to the page you want to auto-reload:

```html
<script src="https://philippn.com/vendor/auto-reload/watch.js"
        reloading-msg="Refreshed"
        timeout="1500"
        watcher-url="/auto-reload/watch.php"
        url-request="key=value"
        reloading-amount="2"
        watch="
        /var/www/html/index.php
        /var/www/html/script.js
        /var/www/html/style.css
"></script>

<link rel="stylesheet" href="https://philippn.com/vendor/auto-reload/styles.css">

<div id="auto-reload__play-pause" onclick="autoReload.playPauseAutoReload()"></div>
```

`/auto-reload/watch.php` - you might use any path depending on where you put unpacked `auto-reload.zip`


# ◦ Auto reload PHP pages after thrown Errors

### Create a separate page and add this:

```html
<script src="https://philippn.com/vendor/auto-reload/ajax-wrapper.js" url="/dev/ajax-wrapper/actual-page.php"></script>

<script src="https://philippn.com/vendor/auto-reload/watch.js"
        watcher-url="auto-reload/watch.php"
        watch="/var/www/html/index.php"
></script>
```

`watcher-url` - might be both relative (if your page is inside the project) or absolute (if your auto-reloading page is outside your project)


### In case you need to execute JavaScript, add this:

```html
<script src="https://philippn.com/vendor/auto-reload/reload-scripts.js"></script>
```


### You can also prepend and append original HTML using these elements:

```html
<div id="auto-reload__prepend">
    <!-- Anything you want to get at the beginning of the page -->
</div>

<div id="auto-reload__append" style="visibility:hidden"> <!-- In case you do not want to see its contents before actual page is loaded -->
    <!-- Anything you want to get at the end of the page -->
</div>
```

### In order to change play/pause button position and appearance (CSS), you may add your own style:

```html
<!-- Either prepend or append -->
<div id="auto-reload__prepend">
    <style>
        #auto-reload__play-pause{
            top: 0 !important;
            left: 0 !important;
            filter: invert(100%) !important;
        }
    </style>
</div>
```

### In case you do not want to execute prepended/appended `<script>` before a target page content is loaded:

This helps to avoid obsolete `undefined variable` JavaScript errors:

```html
<div id="auto-reload__prepend">
    <script-ignore>
        // ...
    </script-ignore>
</div>
```

### Trigger `window.onload` event

```html
<div id="auto-reload__append" style="visibility:hidden">
    <script>
        dispatchEvent(new Event('load')); // triggered for window.onload initializations
    </script>
</div>
```

# ◦ `auto-reload.js`, `ajax-wrapper.js` & `reload-scripts.js` combined into one script - `ajax-watch.js`

```html
<script
        src="https://philippn.com/vendor/auto-reload/ajax-watch.js"
        url="actual/page/to/work/with"
        watcher-url="/auto-reload/watch.php"
        reloading-amount="2"
        reloading-msg="Refreshed"
        timeout="1500"
        url-request="key=value"
        watch="
            Bunch
            of
            file
            paths
"
></script>
```