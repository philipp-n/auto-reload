const autoReload = {
    settings: {
        watch: document.currentScript.getAttribute('watch'),
        reloadingAmount: document.currentScript.getAttribute('reloading-amount') ?? 1,
        timeout: document.currentScript.getAttribute('timeout') ?? 2500,
        watcherUrl: document.currentScript.getAttribute('watcher-url') ?? "/vendor/philippn/auto-reload/php/watch.php",
        urlRequestString: document.currentScript.getAttribute('url-request'),
        reloadingMessage: document.currentScript.getAttribute('reloading-msg') ?? 'Auto reloaded',
    },
    filesToWatch: [],
    count: 0,
    pause: false,
    setFilesToWatch: function () {
        this.filesToWatch = this.settings.watch.split("\n");

        for (let i = this.filesToWatch.length - 1; i >= 0; i--) {
            var trimmed = this.filesToWatch[i].trim();
            if (trimmed === '') {
                this.filesToWatch.splice(i, 1);
            } else {
                this.filesToWatch[i] = trimmed;
            }
        }
    },
    setInterval: function () {
        var _this = this;

        setInterval(function () {
            if (this.count === 1) {
                console.log(_this.settings.reloadingMessage); // @todo Test separately
            }

            if (_this.pause) {
                return;
            }

            var fullWatcherUrl = _this.settings.watcherUrl + "?" +
                _this.settings.urlRequestString +
                "&reloading-amount=" + _this.settings.reloadingAmount +
                "&watch=" + encodeURIComponent(_this.filesToWatch) +
                // Used for disabling cache
                "&auto-reload-timestamp=" + Date.now();

            fetch(fullWatcherUrl, {
                cache: 'no-store',
                headers: {
                    'Cache-Control': 'no-cache'
                }
            })
                .then(function (response) {
                    return response.json();
                })
                .then(function (reload) {
                    if (reload === true) {
                        window.location.reload();
                    }
                });

            _this.count++
        }, _this.settings.timeout);
    },
    init: function () {
        this.setFilesToWatch();
        this.setInterval();
    },
    playPauseAutoReload: function () {
        console.log("Auto-reload paused:", !this.pause);
        this.pause = !this.pause;
    }
};

autoReload.init();