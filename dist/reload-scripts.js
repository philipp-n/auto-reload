// @link https://stackoverflow.com/questions/1197575/can-scripts-be-inserted-with-innerhtml Author: mjs

var reloadScripts = {
    scripts: [],
    isScript: function (node) {
        return node.tagName === 'SCRIPT';
    },
    cloneScriptNode: function (node) {
        var script = document.createElement("script");
        script.text = node.innerHTML;

        var i = -1, attrs = node.attributes, attr;
        while (++i < attrs.length) {
            script.setAttribute((attr = attrs[i]).name, attr.value);
        }
        return script;
    },
    collectScriptNodes: function (node) {
        if (this.isScript(node) === true) {
            this.scripts.push(node);
        } else {
            var i = -1, children = node.childNodes;
            while (++i < children.length) {
                this.collectScriptNodes(children[i]);
            }
        }

        return node;
    },
    replaceNode: function (node) {
        var newNode = this.cloneScriptNode(node);

        return new Promise(function (resolve) {
            node.parentNode.replaceChild(newNode, node);

            if(typeof node.attributes.src !== "undefined"){
                newNode.onload = function () {
                    resolve();
                }
            } else {
                resolve();
            }

        });
    },
    init: function(){

        this.collectScriptNodes(document.getElementsByTagName("body")[0]);

        var  _this = this, i = 0, counted = this.scripts.length;

        function recursive(i){
            _this.replaceNode(_this.scripts[i]).then(
                function(){
                    i++;
                    if(i < counted){
                        recursive(i);
                    }
                }
            );
        }

        recursive(i);
    }
};

