var ajaxWrapper = {
    call: function (currentScript) {
        var ajaxWrapperUrl = currentScript.getAttribute('url');

        // Used for disabling cache
        if (!ajaxWrapperUrl.includes('?')) {
            ajaxWrapperUrl += "?";
        } else {
            ajaxWrapperUrl += "&";
        }

        ajaxWrapperUrl += "auto-reload-timestamp=" + Date.now();

        fetch(ajaxWrapperUrl, {
            cache: 'no-store',
            headers: {
                'Cache-Control': 'no-cache'
            }
        })
            .then(function (response) {
                return response.text();
            })
            .then(function (originalHtml) {

                var prependHtml = "";
                var prependElem = document.getElementById("auto-reload__prepend");
                if (typeof prependElem !== 'undefined' && prependElem !== null) {
                    prependHtml += prependElem.innerHTML
                        .replace(/<script-ignore>/g, '<script>')
                        .replace(/<\/script-ignore>/g, '</script>')
                        .replace(/&gt;/g, '>')
                        .replace(/&lt;/g, '<');
                }

                var appendHtml = "";
                var appendElem = document.getElementById("auto-reload__append");
                if (typeof appendElem !== 'undefined' && appendElem !== null) {
                    appendHtml += appendElem.innerHTML
                        .replace(/<script-ignore>/g, '<script>')
                        .replace(/<\/script-ignore>/g, '</script>')
                        .replace(/&gt;/g, '>')
                        .replace(/&lt;/g, '<');

                }


                document.body.innerHTML =
                    prependHtml +
                    '<link rel="stylesheet" href="https://philippn.com/vendor/auto-reload/styles.css">' +
                    '<div id="auto-reload__play-pause" onclick="autoReload.playPauseAutoReload()"></div>' +
                    originalHtml
                    + appendHtml;


                if (typeof reloadScripts !== 'undefined') {
                    reloadScripts.init();
                }

            })
            .catch(function (error) {
                console.error(error);
            });
    }
}

window.onload = ajaxWrapper.call(document.currentScript);