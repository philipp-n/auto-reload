const ajaxAutoReload = {
    settings: {
        url: document.currentScript.getAttribute('url'),
        watch: document.currentScript.getAttribute('watch'),
        reloadingAmount: document.currentScript.getAttribute('reloading-amount') ?? 1,
        timeout: document.currentScript.getAttribute('timeout') ?? 2500,
        watcherUrl: document.currentScript.getAttribute('watcher-url') ?? "/vendor/philippn/auto-reload/php/watch.php",
        urlRequestString: document.currentScript.getAttribute('url-request'),
        c: document.currentScript.getAttribute('reloading-msg') ?? 'Auto reloaded',
    },
    init: function () {
        var js = document.createElement("script"), _this = this;

        js.src = "https://philippn.com/vendor/js-helper/0.0.2/dist.js";

        document.body.appendChild(js);

        js.onload = function () {
            new jsHelper().includeJsScripts([
                {
                    src: "https://philippn.com/vendor/auto-reload/ajax-wrapper.js",
                    url: _this.settings.url
                },
                {
                    src: "https://philippn.com/vendor/auto-reload/reload-scripts.js"
                },
                {
                    src: "https://philippn.com/vendor/auto-reload/watch.js",
                    'watcher-url': _this.settings.watcherUrl,
                    'reloading-amount': _this.settings.reloadingAmount,
                    watch: _this.settings.watch,
                    timeout: _this.settings.timeout,
                    urlRequestString: _this.settings.urlRequestString,
                    reloadingMessage: _this.settings.reloadingMessage,
                }
            ]);
        }
    }
}

window.onload = function () {
    ajaxAutoReload.init();
}